Kubernetes for Development
**************************

Documentation can be found here:

- https://www.kbsoftware.co.uk/docs/dev-kubernetes.html
- https://www.kbsoftware.co.uk/docs/dev-kubernetes-install.html

.. note:: To convert the scripts to python, we could use
          https://github.com/kubernetes-client/python

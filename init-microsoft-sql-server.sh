#!/bin/bash
# exit immediately if a command exits with a nonzero exit status.
set -e
# treat unset variables as an error when substituting.
set -u
# https://github.com/helm/charts/tree/master/stable/mssql-linux
helm install stable/mssql-linux --name kb-microsoft \
    --set acceptEula.value=Y \
    --set edition.value=Developer \
    --set persistence.enabled=true \
    --set service.type=NodePort

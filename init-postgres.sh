#!/bin/bash
# exit immediately if a command exits with a nonzero exit status.
set -e
# treat unset variables as an error when substituting.
set -u
helm install \
    --name kb-dev-db bitnami/postgresql \
    --set service.nodePort=30432 \
    --set service.type=NodePort \
    --set postgresqlPassword=postgres \
    --set image.tag=12

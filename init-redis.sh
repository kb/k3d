#!/bin/bash
# exit immediately if a command exits with a nonzero exit status.
set -e
# treat unset variables as an error when substituting.
set -u
helm install stable/redis --name kb-redis --set master.service.nodePort=30379 --set master.service.type=NodePort --set usePassword=false --set cluster.enabled=false
